﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DotNetBrowser;
using DotNetBrowser.WinForms;
using System.Windows.Forms;
using DotNetBrowser.WPF;


namespace GOTWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            BrowserView webView;
            InitializeComponent();
            webView = new WPFBrowserView();
            browserControl.Children.Add((UIElement)webView.GetComponent());
            webView.Browser.LoadURL("http://viewers-guide.hbo.com/game-of-thrones/season-6/episode-10/map/location/27/kings-landing");
            //   BrowserView browserView = new WinFormsBrowserView();
            //     browserView.Browser.LoadURL("http://viewers-guide.hbo.com/game-of-thrones/season-6/episode-10/map/location/27/kings-landing");
        }

        private void ButtonFindClick(object sender, RoutedEventArgs e)
        {
            listBox.Items.Clear();

            System.Net.WebClient client = new System.Net.WebClient();
            Stream stream = client.OpenRead("https://api.got.show/api/book/characters");
            StreamReader sr = new StreamReader(stream);


            string txt = sr.ReadLine();


            List<RootObject> roots = JsonConvert.DeserializeObject<List<RootObject>>(txt);
           
            for(int i =0;i < roots.Count;i++)
            {
                if(roots[i].Name.Contains(textName.Text))
                {

                listBox.Items.Add(roots[i].Name);
                }
                
            }

            
             /* <TextBox x:Name="textName" HorizontalAlignment="Left" Height="23" Margin="23,45,0,0" TextWrapping="Wrap" Text="" VerticalAlignment="Top" Width="149"/>
        <Label Content="Введите имя персонажа: " HorizontalAlignment="Left" Margin="23,14,0,0" VerticalAlignment="Top"/>
        <ListBox x:Name="listBox" HorizontalAlignment="Left" Height="218" Margin="23,73,0,0" VerticalAlignment="Top" Width="149" Cursor="None"/>
        <Button Content="Искать" HorizontalAlignment="Left" Margin="172,45,0,0" VerticalAlignment="Top" Width="75" Height="23" Click="ButtonFindClick"/>
        <WebBrowser x:Name="browserControl" HorizontalAlignment="Left" Height="257" Margin="389,45,0,0" VerticalAlignment="Top" Width="282"/>
             */
        }
        

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

           string name =  listBox.SelectedItem.ToString();
            System.Net.WebClient client = new System.Net.WebClient();
            Stream stream = client.OpenRead("https://api.got.show/api/book/characters");
            StreamReader sr = new StreamReader(stream);

            string txt = sr.ReadLine();


            List<RootObject> roots = JsonConvert.DeserializeObject<List<RootObject>>(txt);
            int index = 0;
            for (int i = 0; i < roots.Count; i++)
            {
                if (roots[i].Name == name)
                {

                    index = i;
                }
            }
            if(roots[index].Image != null)
            {
            imagePerson.Source = new BitmapImage(
            new Uri(roots[index].Image));
            }
            else
            {
                imagePerson.Source = new BitmapImage(
            new Uri(@"https://upload.wikimedia.org/wikipedia/commons/3/3d/%D0%9D%D0%B5%D1%82_%D0%B8%D0%B7%D0%BE%D0%B1%D1%80%D0%B0%D0%B6%D0%B5%D0%BD%D0%B8%D1%8F.jpg"));
            }
            System.Windows.MessageBox.Show("Информация о :"+roots[index].Name +" \n" +

            
             "Plod: " + roots[index].Plod + "\n" +          
             "PlodB: " + roots[index].PlodB + "\n" +
             "PlodC: " + roots[index].PlodC + "\n" +          
             "Name: " + roots[index].Name + "\n" +
             "Slug: " + roots[index].Slug + "\n" +
             "Gender:" + roots[index].Gender + "\n" +
             "Culture: " + roots[index].Culture + "\n" +
             "House: " + roots[index].House + "\n" +
             "Alive: " + roots[index].Alive.ToString() + "\n" +
             "CreatedAt: " + roots[index].CreatedAt + "\n" +
             "UpdatedAt: " + roots[index].UpdatedAt + "\n" +
             "Pagerank:  " + roots[index].Pagerank + "\n" +
             "Id: " + roots[index].Id + "\n" +
             "Image:  " + roots[index].Image + "\n" +
             "Birth: " + roots[index].Birth + "\n" +
             "PlaceOfDeath: " + roots[index].PlaceOfDeath + "\n" +
             "Death:  " + roots[index].Death + "\n" +
             "PlaceOfBirth: " + roots[index].PlaceOfBirth + "\n" +
             "LongevityStartB:  " + roots[index].LongevityStartB + "\n" +
             "Father:  " + roots[index].Father + "\n" +
             "Mother:  " + roots[index].Mother + "\n" +
             "Heir:  " + roots[index].Heir + "\n" 
            );
            



        }
    }
}
