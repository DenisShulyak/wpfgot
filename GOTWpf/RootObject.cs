﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOTWpf
{
    public class Pagerank
    {
        public string Title { get; set; }
        public int Rank { get; set; }
    }

    public class RootObject
    {
        public List<object> Titles { get; set; }
        public List<object> Spouse { get; set; }
        public List<object> Children { get; set; }
        public List<object> Allegiance { get; set; }
        public List<object> Books { get; set; }
        public int          Plod { get; set; }
        public List<object> Longevity { get; set; }
        public double       PlodB { get; set; }
        public int          PlodC { get; set; }
        public List<object> LongevityB { get; set; }
        public List<object> LongevityC { get; set; }
        public string       _id { get; set; }
        public string       Name { get; set; }
        public string       Slug { get; set; }
        public string       Gender { get; set; }
        public string       Culture { get; set; }
        public string       House { get; set; }
        public bool         Alive { get; set; }
        public DateTime     CreatedAt { get; set; }
        public DateTime     UpdatedAt { get; set; }
        public int          __v { get; set; }
        public Pagerank     Pagerank { get; set; }
        public string       Id { get; set; }
        public string       Image { get; set; }
        public int?         Birth { get; set; }
        public string       PlaceOfDeath { get; set; }
        public int?         Death { get; set; }
        public string       PlaceOfBirth { get; set; }
        public int?         LongevityStartB { get; set; }
        public string       Father { get; set; }
        public string       Mother { get; set; }
        public string       Heir { get; set; }
    }
}
